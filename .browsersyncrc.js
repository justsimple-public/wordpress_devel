// Full list of options:
// http://www.browsersync.io/docs/options/

module.exports = function () {
    const config = require('./package.json').gulp;
    const themePath = 'example/wp/themes/' + config.theme;

    return {
        "server": {
            "baseDir": [themePath],
        },
        "https": false,
        "injectChanges": true,
        "notify": true,
        "open": true,
        "port": 3000,
        "reloadThrottle": 300,
        "reloadDelay": 300,
        "browser": "google chrome",
    }
}
