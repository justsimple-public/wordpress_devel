const fs = require('fs');
const gulp = require('gulp');
const runSequence = require('run-sequence');

// Project plugins
const rev = require('gulp-rev');
const sass = require('gulp-sass');
const clean = require('gulp-clean');
const concat = require('gulp-concat');
const rename = require('gulp-rename');
const replace = require('gulp-replace-task');
const collect = require('gulp-rev-collector');
const imagemin = require('gulp-imagemin');
const pngquant = require('imagemin-pngquant');
const uglify = require('gulp-uglify');
const minifyCss = require('gulp-minify-css');
const awspublish = require('gulp-awspublish');
const autoprefixer = require('gulp-autoprefixer');
const BrowserSync = require("browser-sync");
const browserSyncConfig = require("./.browsersyncrc.js");


// Configuration
const browserSync = BrowserSync.create();
const config = require('./package.json').gulp;
const themePath = 'example/wp/themes/' + config.theme;
const develPath = config.develPath + config.theme;
const distPath = 'dist/' + config.theme;
const distPathAbsolute = '/' + distPath;
const assetPath = develPath + '/assets';

gulp.task(
    'theme',
    [
        'compile-styles',
        'compile-scripts',
        'compile-fonts',
        'compile-images',
        'compile-templates',
        'serve'
    ],
    function () {
        gulp.watch(assetPath + '/scss/**/*.scss', ['compile-styles']);
        gulp.watch(assetPath + '/js/**/*.js', ['compile-scripts']);
        gulp.watch(assetPath + '/fonts/**/*', ['compile-fonts']);
        gulp.watch(assetPath + '/images/**/*', ['compile-images']);
        gulp.watch(assetPath + '/templates/**/*', ['compile-templates']);
    }
);

gulp.task(
    'theme-css',
    [
        'compile-css',
        'compile-scripts',
        'compile-fonts',
        'compile-images',
        'compile-templates',
        'serve'
    ],
    function () {
        gulp.watch(assetPath + '/scss/**/*.scss', ['compile-css']);
        gulp.watch(assetPath + '/js/**/*.js', ['compile-scripts']);
        gulp.watch(assetPath + '/fonts/**/*', ['compile-fonts']);
        gulp.watch(assetPath + '/images/**/*', ['compile-images']);
        gulp.watch(assetPath + '/templates/**/*', ['compile-templates']);
        browserSync.reload();
    }
);

gulp.task('deploy-assets', function (callback) {
    runSequence(
        'clean',
        ['compile-styles', 'compile-scripts', 'compile-images', 'compile-fonts', 'compile-templates'],
        ['optimize-styles', 'optimize-scripts', 'optimize-images'],
        'version-assets',
        ['replace-versioned-assets-in-assets', 'replace-versioned-assets-in-templates'],
        'gzip-assets',
        'publish-to-s3',
        callback
    );
});

gulp.task('clean', function () {
    return gulp.src([
        distPath,
        themePath + '/*.php'
    ], {read: false})
        .pipe(clean({force: true}));
});

// Styles
// ------

gulp.task('compile-css', function () {
    return (
        gulp.src([
            assetPath + '/css/style.css'
        ])
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(gulp.dest(distPath))
            .pipe(gulp.dest(themePath))
            .pipe(browserSync.stream())
    );
});


gulp.task('compile-styles', function () {
    return (
        gulp.src([
            assetPath + '/scss/style.scss'
        ])
            .pipe(sass().on('error', sass.logError))
            .pipe(autoprefixer({
                browsers: ['last 2 versions'],
                cascade: false
            }))
            .pipe(gulp.dest(distPath))
            .pipe(gulp.dest(themePath))
            .pipe(browserSync.stream())
    );
});

gulp.task('optimize-styles', function () {
    return gulp.src(distPath + 'style.css')
        .pipe(minifyCss())
        .pipe(gulp.dest(distPath));
});


// Scripts
// -------

gulp.task('compile-scripts', function () {
    return (
        gulp.src([
            assetPath + '/js/script.js'
        ])
            .pipe(concat('script.js'))
            .pipe(gulp.dest(distPath + '/js'))
            .pipe(gulp.dest(themePath + '/js'))
            .pipe(browserSync.stream())
    );
});

gulp.task('optimize-scripts', function () {
    return gulp.src(distPath + '/js/script.js')
        .pipe(uglify())
        .pipe(gulp.dest(distPath + '/js/'));
});


// Images
// -------

gulp.task('compile-images', function () {
    return gulp.src(assetPath + '/images/**/*')
        .pipe(gulp.dest(distPath + '/images'))
        .pipe(gulp.dest(themePath + '/images'))
        .pipe(browserSync.stream())
});

gulp.task('optimize-images', function () {
    return gulp.src(distPath + '/images/**/*')
        .pipe(imagemin({
            progressive: true,
            svgoPlugins: [{removeViewBox: false}],
            use: [pngquant()]
        }))
        .pipe(gulp.dest(distPath + '/images'));
});


// Fonts
// -----

gulp.task('compile-fonts', function () {
    return gulp.src(assetPath + '/fonts/**/*')
        .pipe(gulp.dest(distPath + '/fonts'))
        .pipe(gulp.dest(themePath + '/fonts'))
        .pipe(browserSync.stream())

});


// Templates
// ---------

gulp.task('compile-templates', function () {
    return gulp.src(assetPath + '/templates/**/*')
        .pipe(gulp.dest(themePath))
        .pipe(browserSync.stream())
});


// versioning
// -----------

gulp.task('version-assets', function () {
    return gulp.src(distPath + '/**/*')
        .pipe(rev())
        .pipe(gulp.dest(distPath))
        .pipe(rev.manifest())
        .pipe(gulp.dest(themePath))
        .pipe(browserSync.stream())
});

gulp.task('replace-versioned-assets-in-assets', function () {
    const dirReplacements = {};
    dirReplacements[distPathAbsolute] = config.productionAssetURL;

    return gulp.src([
        themePath + '/**/*.json',
        distPath + '/**/*.css',
        distPath + '/**/*.js'
    ])
        .pipe(collect({replaceReved: true, dirReplacements: dirReplacements}))
        .pipe(gulp.dest(distPath));
});

gulp.task('replace-versioned-assets-in-templates', function () {
    const dirReplacements = {};
    dirReplacements[distPathAbsolute] = config.productionAssetURL;

    return gulp.src([
        themePath + '/**/*.json',
        themePath + '/*.php'
    ])
        .pipe(collect({replaceReved: true, dirReplacements: dirReplacements}))
        .pipe(gulp.dest(themePath));
});

//SERVE
// browser-sync task for starting the server.
gulp.task('serve', function() {
    //watch files
    var files = [
        themePath + '**/*'
    ];

    //initialize browsersync
    browserSync.init(files, {
        //browsersync with a php server
        proxy: "localhost:8080",
        notify: false
    });
    gulp.watch('./src/scss/**/*.scss', ['sass']);

    // gulp.task('default', gulpSequence(['fonts', 'sass', 'js', 'watch']));
});


// S3
// --

gulp.task('gzip-assets', function () {
    return gulp.src([
        '!' + distPath + '/**/*.gz',
        distPath + '/**/*'
    ])
        .pipe(awspublish.gzip({ext: '.gz'}))
        .pipe(gulp.dest(distPath));
});

gulp.task('publish-to-s3', function () {
    const publisher = awspublish.create(config.aws);
    const headers = {
        'Cache-Control': 'max-age=31536000, no-transform, public'
    };

    return gulp.src(distPath + '/**')
        .pipe(publisher.publish(headers))
        .pipe(publisher.sync())
        .pipe(awspublish.reporter());
});
